const until = protractor.ExpectedConditions;

class loginPage {

	constructor() {
		this.timeout = 5000;

		this.btnShowAsList = element(by.className('Common-ActionMenu-Container')).element(by.xpath('//i[text()="list"]'));
		this.btnShowAsModule = element(by.className('Common-ActionMenu-Container')).element(by.xpath('//i[text()="view_module"]'));

		this.documentsList = element(by.id('archive-list'));
		this.documentsListTable = this.documentsList.element(by.css('table.md-data-table'));
		this.documentsListModules = element(by.css('.archive-list'));

		this.sectionDocumentsInfoPopup = element(by.id('archive-content-of-package-dialog'));
		this.titleDocumentsInfoPopup = element(by.id('archive-content-of-package-dialog-title'));
		this.btnCloseDocumentsInfoPopup = element(by.className('md-btn--dialog'));

		this.btnOpenFilters = element(by.xpath('//i[text()="keyboard_backspace"]'));

		this.inputSearchDoc = element(by.id('filter-searchDocNo-docNo'));
		this.btnSelectClient = element(by.id('autosuggest-company-multiple-select'));
		this.btnDateFrom = element(by.id('filter-period-dateFrom'));
		this.btnDateTo = element(by.id('filter-period-dateTo'));
		this.btnUpdatedFrom = element(by.id('filter-updated-timestampsql'));
		this.btnSelectDocType = element(by.id('autosuggest-doctype-multiple-select'));
		this.btnSelectStatus = element(by.id('autosuggest-status-multiple-select'));
		this.btnFind = element(by.css('.Common-FilterPanel-Content button[type="submit"]'));
		this.btnClear = element(by.xpath('//button[text()="Очистить"]'));

		this.stateFilterClosed = element(by.className('layout-content--filter-panel_closed'));
		this.loader = element(by.className('page-loading-display'));
	}

	waitPageLoaded() {
		browser.wait(until.visibilityOf(this.loader), this.timeout);
		browser.wait(until.not(until.visibilityOf(this.loader)), this.timeout);
	}

	async openFilters() {
		if (await this.stateFilterClosed.isPresent()) {
			browser.wait(until.elementToBeClickable(this.btnOpenFilters), this.timeout);
			this.btnOpenFilters.click();
			browser.sleep(500);
		}
	}

	checkAllFilters() {
		browser.wait(until.visibilityOf(this.inputSearchDoc), this.timeout);
		expect(this.inputSearchDoc.getAttribute('placeholder')).toEqual('введите значение для поиска');

		browser.wait(until.visibilityOf(this.btnSelectClient), this.timeout);
		expect(this.btnSelectClient.getAttribute('placeholder')).toEqual('Возможные юр. лица');

		browser.wait(until.visibilityOf(this.btnDateFrom), this.timeout);
		expect(this.btnDateFrom.getAttribute('placeholder')).toEqual('С');

		browser.wait(until.visibilityOf(this.btnDateTo), this.timeout);
		expect(this.btnDateTo.getAttribute('placeholder')).toEqual('По');

		browser.wait(until.visibilityOf(this.btnUpdatedFrom), this.timeout);
		expect(this.btnUpdatedFrom.getAttribute('placeholder')).toEqual('С');

		browser.wait(until.visibilityOf(this.btnSelectDocType), this.timeout);
		expect(this.btnSelectDocType.getAttribute('placeholder')).toEqual('Возможные типы документов');

		browser.wait(until.visibilityOf(this.btnSelectStatus), this.timeout);
		expect(this.btnSelectStatus.getAttribute('placeholder')).toEqual('Возможные статусы');
	}

	showListStructure() {
		browser.wait(until.elementToBeClickable(this.btnShowAsList), this.timeout);
		this.btnShowAsList.click();
	}

	showModuleStructure() {
		browser.wait(until.elementToBeClickable(this.btnShowAsModule), this.timeout);
		this.btnShowAsModule.click();
	}

	typeSearchDoc(val) {
		browser.wait(until.elementToBeClickable(this.inputSearchDoc), this.timeout);
		this.inputSearchDoc.sendKeys(val);
	}

	selectClient(val) {
		let el = element(by.xpath("//div[@id='react-autowhatever-company-multiple-select']//div[text()='" + val + "']"));

		browser.wait(until.elementToBeClickable(this.btnSelectClient), this.timeout);
		this.btnSelectClient.click();

		this.btnSelectClient.sendKeys(val);
		browser.wait(until.elementToBeClickable(el), this.timeout);
		el.click();
	}

	selectDateFrom() {

	}

	selectDateTo() {

	}

	selectUpdatedFrom(day) {
		let el = element(by.xpath('//button[contains(@class, "md-calendar-date--btn")]//span[text()="' + day + '"]'));
		let elConfirm = element(by.xpath('//button[text()="ОК"]'));

		browser.wait(until.elementToBeClickable(this.btnUpdatedFrom), this.timeout);
		this.btnUpdatedFrom.click();

		browser.wait(until.elementToBeClickable(el), this.timeout);
		el.click();

		browser.wait(until.elementToBeClickable(elConfirm), this.timeout);
		elConfirm.click();

		browser.wait(until.not(until.visibilityOf(el)), this.timeout);
	}

	selectDocType(val) {
		let el = element(by.xpath("//div[@id='react-autowhatever-doctype-multiple-select']//div[text()='" + val + "']"));

		browser.wait(until.elementToBeClickable(this.btnSelectDocType), this.timeout);
		this.btnSelectDocType.click();

		browser.wait(until.elementToBeClickable(el), this.timeout);
		el.click();
	}

	selectStatus(val) {
		let el = element(by.xpath("//div[@id='react-autowhatever-status-multiple-select']//div[text()='" + val + "']"));

		browser.wait(until.elementToBeClickable(this.btnSelectStatus), this.timeout);
		this.btnSelectStatus.click();

		browser.wait(until.elementToBeClickable(el), this.timeout);
		el.click();
	}

	find() {
		browser.wait(until.elementToBeClickable(this.btnFind), this.timeout);
		this.btnFind.click();
	}

	clear() {
		browser.wait(until.elementToBeClickable(this.btnClear), this.timeout);
		this.btnClear.click();
	}

	getDocumentLineElement(docNum, docType) {
		return this.documentsListTable.element(by.xpath('//div[text()="' + docNum + '"]/../following-sibling::td/div[text()="' + docType + '"]/../..'))
	}

	getDocumentModuleElement(docNum, docType) {
		return this.documentsListModules.element(by.xpath('//div[text()="' + docType + '"]/../../../following-sibling::div//div[text()="' + docNum + '"]/../../../..'));
	}

	checkDocumentExistsListView(docNum, docType) {
		let documentLineEl = this.getDocumentLineElement(docNum, docType);

		browser.wait(until.visibilityOf(documentLineEl), this.timeout);
	}

	checkDocumentNotExistsListView(docNum, docType) {
		let documentLineEl = this.getDocumentLineElement(docNum, docType);

		browser.wait(until.not(until.visibilityOf(documentLineEl)), this.timeout);
	}

	checkDocumentExistsModuleView(docNum, docType) {
		let el = this.getDocumentModuleElement(docNum, docType);

		browser.wait(until.visibilityOf(el), this.timeout);
	}

	checkDocumentNotExistsModuleView(docNum, docType) {
		let el = this.getDocumentModuleElement(docNum, docType);

		browser.wait(until.not(until.visibilityOf(el)), this.timeout);
	}

	downloadOneDocument(docNum, docType) {
		let documentLineEl = this.getDocumentLineElement(docNum, docType);

		return documentLineEl.all(by.xpath('td//i[text()="file_download"]')).first().click();
	}

	showDocumentsInfo(docNum, docType) {
		let documentLineEl = this.getDocumentLineElement(docNum, docType);
		let el = documentLineEl.element(by.xpath('td//i[text()="info"]'));

		browser.wait(until.visibilityOf(el), this.timeout);

		return el.click();
	}

	checkCommonDocumentsInfoPopup() {
		browser.wait(until.visibilityOf(this.titleDocumentsInfoPopup), this.timeout);
		browser.wait(until.visibilityOf(this.btnCloseDocumentsInfoPopup), this.timeout);
	}

	checkDocumentsInfoPopup(docNum, docType, docDate) {
		let infoEl = this.sectionDocumentsInfoPopup.element(by.xpath('//p[contains(., "' + docType + ' — ' + docNum + ' — ' + docDate + '")]'));
		browser.wait(until.visibilityOf(infoEl), this.timeout);
	}
}

module.exports = new loginPage();