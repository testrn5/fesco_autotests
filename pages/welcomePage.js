class welcomePage {

	openLoginPage() {
		browser.get('/login')
	}

	openArchivePage() {
		browser.get('/archive')
	}
}

module.exports = new welcomePage();