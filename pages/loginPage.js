const until = protractor.ExpectedConditions;

class loginPage {

	constructor() {
		this.timeout = 5000;

		this.inputLogin = element(by.id('formField-usernameOrEmail'));
		this.inputPassword = element(by.id('formField-password'));
		this.btnAuth = element(by.xpath('//a[@href="/forgot"]/preceding-sibling::button'));
	}

	userAuth(login, password) {
		browser.wait(until.visibilityOf(this.inputLogin), this.timeout);
		browser.wait(until.visibilityOf(this.inputPassword), this.timeout);
		browser.wait(until.visibilityOf(this.btnAuth), this.timeout);

		browser.wait(until.elementToBeClickable(this.inputLogin), this.timeout);
		this.inputLogin.sendKeys(login);
		browser.wait(until.textToBePresentInElementValue(this.inputLogin, login));
		browser.wait(until.elementToBeClickable(this.inputPassword), this.timeout);
		this.inputPassword.sendKeys(password);
		browser.wait(until.textToBePresentInElementValue(this.inputPassword, password));
		browser.wait(until.elementToBeClickable(this.btnAuth), this.timeout);
		browser.sleep(300);
		this.btnAuth.click();

		browser.wait(until.not(until.visibilityOf(this.btnAuth)), this.timeout);
	}
}

module.exports = new loginPage();