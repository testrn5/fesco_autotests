const welcomePage = require('pages/welcomePage');
const loginPage = require('pages/loginPage');
const archivePage = require('pages/archivePage');
const tools = require('tools');

describe('Список документов:', function() {
	afterEach(function() {
		browser.driver.manage().deleteAllCookies();
		browser.executeScript('window.localStorage.clear();');
	});

	it('Выгрузка единичного документа', async function() {
		let docNum = 'WYSA2013/191';
		let docType = 'Акт оказанных услуг \\ выполненных работ (исходящий)';

		tools.clearDownloads();

		welcomePage.openLoginPage();
		loginPage.userAuth(browser.params.userData.login, browser.params.userData.password);
		welcomePage.openArchivePage();
		archivePage.waitPageLoaded();

		archivePage.showListStructure();
		archivePage.downloadOneDocument(docNum, docType);

		await browser.wait(tools.havePdfFile, 10000);

		let fileName = tools.getLastFile();

		expect(fileName.startsWith(docNum.replace('/', '_'))).toEqual(true);
	});

	xit('Not covered: Выгрузка пакета документов', function() {});

	it('Информация о документах, которые входят в пакет', function() {
		let docNum = 'WYSA2013/191';
		let docType = 'Акт оказанных услуг \\ выполненных работ (исходящий)';
		let docDate = '05.12.2019';

		welcomePage.openLoginPage();
		loginPage.userAuth(browser.params.userData.login, browser.params.userData.password);
		welcomePage.openArchivePage();
		archivePage.waitPageLoaded();

		archivePage.showListStructure();
		archivePage.showDocumentsInfo(docNum, docType);
		archivePage.checkCommonDocumentsInfoPopup();
		archivePage.checkDocumentsInfoPopup(docNum, docType, docDate);
	});
});
