const welcomePage = require('pages/welcomePage');
const loginPage = require('pages/loginPage');
const archivePage = require('pages/archivePage');

describe('Фильтр или поиск:', function() {
	afterEach(async function() {
		await browser.driver.manage().deleteAllCookies();
		browser.executeScript('window.localStorage.clear();');
	});

	it('Проверка фильтров(выезжает и пропадает, проверка контента внутри фильтров)', async function() {
		welcomePage.openLoginPage();
		loginPage.userAuth(browser.params.userData.login, browser.params.userData.password);
		welcomePage.openArchivePage();
		archivePage.waitPageLoaded();

		await archivePage.openFilters();
		archivePage.checkAllFilters();
	});

	it('Ввод данных поиска и выбор всех фильтров, очистка фильтров в режиме списка', async function() {
		// valid data
		let docNum = 'WYSA2013/191';
		let client = 'ФАРЕС';
		let docType = 'Акт оказанных услуг \\ выполненных работ (исходящий)';
		let status = 'Подписан с двух сторон';

		// should not be present
		let filteredDoc = 'WYSA2055/192';

		welcomePage.openLoginPage();
		loginPage.userAuth(browser.params.userData.login, browser.params.userData.password);
		welcomePage.openArchivePage();
		archivePage.waitPageLoaded();

		archivePage.showListStructure();

		await archivePage.openFilters();

		// Check find with filters
		archivePage.typeSearchDoc(docNum);
		archivePage.selectClient(client);
		archivePage.selectDocType(docType);
		archivePage.selectStatus(status);
		archivePage.find();

		archivePage.checkDocumentExistsListView(docNum, docType);
		archivePage.checkDocumentNotExistsListView(filteredDoc, docType);

		// Check clear find
		archivePage.clear();

		archivePage.checkDocumentExistsListView(docNum, docType);
		archivePage.checkDocumentExistsListView(filteredDoc, docType);
	});

	it('Ввод данных поиска и выбор всех фильтров, очистка фильтров в модульном режиме', async function() {
		// valid data
		let docNum = 'WYSA2013/191';
		let client = 'ФАРЕС';
		let docType = 'Акт оказанных услуг \\ выполненных работ (исходящий)';
		let status = 'Подписан с двух сторон';

		// should not be present
		let filteredDoc = 'WYSA2055/192';

		welcomePage.openLoginPage();
		loginPage.userAuth(browser.params.userData.login, browser.params.userData.password);
		welcomePage.openArchivePage();
		archivePage.waitPageLoaded();

		archivePage.showListStructure();
		archivePage.showModuleStructure();

		await archivePage.openFilters();

		// Check find with filters
		archivePage.typeSearchDoc(docNum);
		archivePage.selectClient(client);
		archivePage.selectUpdatedFrom('11');
		archivePage.selectDocType(docType);
		archivePage.selectStatus(status);
		archivePage.find();

		archivePage.checkDocumentExistsModuleView(docNum, docType);
		archivePage.checkDocumentNotExistsModuleView(filteredDoc, docType);

		// Check clear find
		archivePage.clear();

		archivePage.checkDocumentExistsModuleView(docNum, docType);
		archivePage.checkDocumentExistsModuleView(filteredDoc, docType);
	});
});
