## Install modules
```
1. nodejs 12+
2. npm install -g protractor
3. npm i protractor-jasmine2-screenshot-reporter
4. npm i webdriver-manager update
5. java last version

```

## Start tests
```
set NODE_PATH=.&& protractor conf.js
```