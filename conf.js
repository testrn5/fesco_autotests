let config = require('test_conf');
let HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');

let reporter = new HtmlScreenshotReporter({
	dest: 'output',
	filename: 'report.html'
});

exports.config = {
	baseUrl: 'https://lk-stage.k8s.fesco.com',
    //seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
        browserName: 'chrome',
		chromeOptions: {
			prefs: {
				download: {
					'prompt_for_download': false,
					'directory_upgrade': true,
					'default_directory': config.download_path
				}
			}
		}
    },
    allScriptsTimeout: 90000,
    getPageTimeout: 90000,
    framework: 'jasmine',
	//ignoreSynchronization: true,
	//restartBrowserBetweenTests: true,
    jasmineNodeOpts: {
        defaultTimeoutInterval: 200000
    },
	// Setup the report before any tests start
	beforeLaunch: function() {
		return new Promise(function(resolve){
			reporter.beforeLaunch(resolve);
		});
	},
	// Close the report after all tests finish
	afterLaunch: function(exitCode) {
		return new Promise(function(resolve){
			reporter.afterLaunch(resolve.bind(this, exitCode));
		});
	},
    onPrepare: function () {
		// browser.driver.manage().window().setSize(1920, 1080);
		browser.driver.manage().window().maximize();
		jasmine.getEnv().addReporter(reporter);

		jasmine.DEFAULT_TIMEOUT_INTERVAL = 200000;
		browser.ignoreSynchronization = true;
    },
	onComplete      : function() {
		browser.close();
		browser.quit();
	},
	specs: ['tests/*.js'],
    params: {
        userData: {
        	login: 'testemail+autotest@innervate.ru',
			password: 'Ktulhu77'
		}
    }
};
