let config = require('test_conf');
let fs = require('fs');

module.exports = {
	getLastFile: function() {
		let files = fs.readdirSync(config.download_path);

		return files[0];
	},
	clearDownloads: function() {
		let dir = config.download_path;
		let files = fs.readdirSync(dir);

		for (let i = 0; i < files.length; i++) {
			let filePath = dir + '/' + files[i];

			fs.unlinkSync(filePath);
		}
	},
	havePdfFile: function() {
		var files = fs.readdirSync(config.download_path);

		return files.length > 0 ? files[0].endsWith('.pdf') : false;
	}
};